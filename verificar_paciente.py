import unittest
import paciente_medico

class TestPaciente(unittest.TestCase):
    def test_ver_hyst_cliniq(self):
        self.assertEqual(self.paciente.ver_hyst_cliniq(), "Historial médico")

class TestMedico(unittest.TestCase):
    def test_consulta_agenda(self):
        self.assertEqual(self.medico.consulta_agenda(), ["2024/03/11 10:00", "2024/03/12 11:30"])

if __name__ == '__main__':
    unittest.main()
